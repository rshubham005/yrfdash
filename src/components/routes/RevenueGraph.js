import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'bottom',
    },
    title: {
      display: true,
      text: 'Amount in Cr.',
      position:'left'
    },
  },
};

export default function RevenueGraph1() {
  
const labels = ['-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10','11','12','13','14'];

const data = {
  labels,
  datasets: [
    {
      label: 'War',
      data: [0,3,4,6,7,12,20,25,30,20,10,10,10,9,15,18,18,15,15,15,15,15],
      borderColor: '#3491F8',
    },
    {
      label: 'TZH',
      data: [0,0,0,6,7,12,30,20,25,20,12,12,12,15,15,21,18,15,15,15,15,15],
      borderColor: '#ED7d31',
    },
    {
      label: 'TOH',
      data: [0,0,5,6,8,14,25,30,20,10,7,6,5,4,4,8,8,5,4,2,2,2],
      borderColor: '#a5a5a5',
    },
    {
      label: 'Race 3',
      data: [0,0,2,3,4,5,15,20,20,12,5,4,3,3,4,8,8,5,4,2,2,2],
      borderColor: '#E8B56B',
    },
    {
      label: 'Sooryavanshi',
      data: [2,4,4,5,6,10,15,18,15,16,10,8,7,6,9,10,12,9,8,7,6,6],
      borderColor: '#ffc000',
    },
  ],
};

  return (
  <>
  <div className='filter'>
    <label>Revenue Cr.</label>
  </div>
  <Line options={options} data={data} />
  </>
  )
}
