import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'bottom',
    },
    title: {
      display: true,
      text: 'Number of Tickets',
      position:'left'
    },
  },
};

export default function TicketBooking1() {
  
  const labels = ['-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10','11','12','13','14'];

const data = {
  labels,
  datasets: [
    {
        label: 'War',
        data: [0,20,20,50,100,150,200,300,500,600,700,400,300,200,200,350,450,300,300,250,250,250],
        borderColor: '#3491F8',
      },
      {
        label: 'TZH',
        data: [0,0,0,100,150,250,350,700,900,1200,600,400,350,300,450,450,600,350,300,300,250,350],
        borderColor: '#ED7d31',
      },
      {
        label: 'TOH',
        data: [0,0,200,400,600,750,800,1500,1800,1500,400,300,200,200,450,300,300,150,100,100,90,80],
        borderColor: '#a5a5a5',
      },
      {
        label: 'Race 3',
        data: [0,0,150,200,350,300,400,600,500,600,400,300,200,200,150,300,350,200,150,100,100,100],
        borderColor: '#E8B56B',
      },
      {
        label: 'Sooryavanshi',
        data: [50,70,60,100,120,150,300,500,800,1000,500,400,450,400,500,600,700,350,300,250,150,150
        ],
        borderColor: '#ffc000',
      },
  ],
};

  return (
  <>
  <div className='filter'>
    <label>Ticket Booking Trend</label>
  </div>
  <Line options={options} data={data} />
  </>
  )
}
